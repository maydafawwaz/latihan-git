require 'digest'

module Enkirpsi
    def enk(string)
        Digest::SHA2.hexdigest(string)
    end
end

class User
    include Enkirpsi
    attr_accessor :nama,:password
    def initialize(name,pwd)
        @nama=name
        @password = enk(pwd)
    end
end

orang=User.new("Dira","Novita")
puts orang.nama
puts orang.password

lagi = Enkirpsi.new()