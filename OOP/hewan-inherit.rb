class Hewan
    attr_accessor :jumlah_kaki, :alat_pernafasan, :jenis_hewan
    def info
        "#{self.jenis_hewan} Memiliki #{@jumlah_kaki} kaki dan bernapas dengan #{self.alat_pernafasan}"
    end
end

class Ayam < Hewan

    def initialize
        self.jenis_hewan = "Ayam"
        self.jumlah_kaki = 4
        self.alat_pernafasan = "Paru-paru" 
    end
end

class Sapi < Hewan
   
    def initialize
        self.jenis_hewan = "Sapi"
        self.jumlah_kaki = 4
        self.alat_pernafasan = "Paru-paru" 
    end
end

class Ikan < Hewan
    
    def initialize
        self.jumlah_kaki = 0
        self.alat_pernafasan = "Insang" 
        self.jenis_hewan = "Ikan"
    end
end

h1 = Ayam.new
h2 = Ikan.new
h3 = Sapi.new


puts h1.info
puts h2.info
puts h3.info