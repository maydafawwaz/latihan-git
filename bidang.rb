class Lingkaran
    @radius
    def initialize(r)
        @radius=r
    end
    def luas
        return 3.14*@radius*@radius
    end
end

class Sample
    def hello
       puts "Hello Ruby!"
    end
 end
obj = Sample.new

obj.hello
class PersegiPanjang
    @l
    @p
    def initialize(panjang,lebar)
        @l=lebar
        @p=panjang
    end

    def luas
        @l * @p
    end
end

class Segitiga
    @a
    @t
    def initialize(alas,tinggi)
        @a=alas
        @t=tinggi
    end

    def luas
        (@a*@t)/2
    end
end

puts "Menghitung luas bidang"
puts "menu: "
puts "1. lingkaran"
puts "2. persegi panjang"
puts "3. segitiga"

select = gets.to_i
case select
    when 1
        print "Masukkan jari2: "
        r=gets.to_i
        obj = Lingkaran.new(r)
        puts obj.luas
    when 2
        print "Masukkan Panjang: "
        p=gets.to_i
        print "Masukkan Lebar: "
        l=gets.to_i
        obj = PersegiPanjang.new(p,l)
        puts obj.luas
    when 3
        print "Masukkan alas: "
        a=gets.to_i
        print "Masukkan tinggi: "
        t=gets.to_i
        obj = Segitiga.new(a,t)
        puts obj.luas
end